export enum Tiles {
  EMPTY = '.',
  WALL = '#',
  FLOOR = '@',
  CORRIDOR = '*',
  DOOR = '%',
  STAIRS_UP = '^',
  STAIRS_DOWN = '_',
  OBSTACLE = 'O',
  DECORATIVE = 'D',
  CHEST = 'C',
  NO_PVP_AREA = '-',
  PVP_AREA = '+',
  TRAP = 'T',
  HIDDEN_TRAP = 'H',
  STORE = '$',
  BOSS = 'B',
}

export const surfaceTiles: Tiles[] = [
  Tiles.FLOOR,
  Tiles.DECORATIVE,
  Tiles.CHEST,
  Tiles.NO_PVP_AREA,
  Tiles.PVP_AREA,
  Tiles.STORE,
  Tiles.BOSS,
  Tiles.DOOR,
];
