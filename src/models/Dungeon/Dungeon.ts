//Interfaces
import { IDungeon } from './interfaces/IDungeon';
import { IDungeonConfig } from './interfaces/IDungeonConfig';

import { IGeneratedMap } from '../Map/interfaces/IGeneratedMap';
import { IMapConfig } from '../Map/interfaces/IMapConfig';
import { IMap } from '../Map/interfaces/IMap';

import { ITimer } from '../Timer/interfaces/ITimer';
import { IRandom } from '../Random/interfaces/IRandom';

//Models
import { Map } from '../Map/Map';
import { Timer } from '../Timer/Timer';
import { Random } from '../Random/Random';
import { DungeonType } from './DungeonType';

export class DungeonGenerator implements IDungeon {
  public config: IDungeonConfig;
  public Timer: ITimer;
  public Random: IRandom;

  public maps: IGeneratedMap[];

  public constructor(config?: IDungeonConfig) {
    this.config = config || DungeonGenerator.getDefaultConfig();
    this.maps = [];

    this.Timer = new Timer;
    this.Random = new Random;
  }

  public setConfig(config: IDungeonConfig): this {
    this.config = config;
    return this;
  }

  public generate(): this {
    this.Timer.start();
    this.generateMaps();
    this.Timer.end();

    console.log(`\nAll jobs done in ${this.getGenerationTime()}s.`);

    return this;
  }

  public generateMapConfig(currentLevel: number): IMapConfig {
    return {
      currentMap: currentLevel,
      mapsToCreate: this.config.levels.amount,
      rooms: this.Random.integer(this.config.maps.rooms.min, this.config.maps.rooms.max),
      roomsSize: this.config.rooms.size,
      minimalRoomsPlaced: this.config.rooms.min,
      mapSize: {
        width: this.Random.integer(this.config.maps.size.width.min, this.config.maps.size.width.max),
        height: this.Random.integer(this.config.maps.size.height.min, this.config.maps.size.height.max),
      },
      chances: {
        maxRange: this.config.rooms.chances.maxRange,
        store: this.Random.integer(this.config.rooms.chances.store.min, this.config.rooms.chances.store.max),
        pvpArea: this.Random.integer(this.config.rooms.chances.pvpArea.min, this.config.rooms.chances.pvpArea.max),
        nonPvpArea: this.Random.integer(this.config.rooms.chances.nonPvpArea.min, this.config.rooms.chances.nonPvpArea.max),
        bossArea: this.Random.integer(this.config.rooms.chances.bossArea.min, this.config.rooms.chances.bossArea.max),
      },
      surface: {
        createStairsUpOnFirstLevel: this.config.maps.surface.createStairsUpOnLastLevel,
        createStairsDownOnLastLevel: this.config.maps.surface.createStairsDownOnLastLevel,
        obstacles: this.config.maps.surface.obstacles,
        chests: this.config.maps.surface.chests,
      },
      maxTries: this.config.dungeon.maxTries,
      allowRoomsToOverlap: this.config.maps.allowRoomsToOverlap,
      doCorridors: this.config.dungeon.type === DungeonType.WITH_CORRIDORS,
    };
  }

  public getGenerationTime(): string {
    return this.Timer.getReadableDifference();
  }

  public getMaps(): IGeneratedMap[] {
    return this.maps;
  }

  private generateMaps(): void {
    for (let currentLevel: number = 1; currentLevel <= this.config.levels.amount; currentLevel++) {
      const NewMap: IMap = new Map(currentLevel, this.generateMapConfig(currentLevel));
      const map: IGeneratedMap = NewMap.generate().getMap();

      this.maps.push(map);

      console.log(`> map ${currentLevel} generated in ${map.generationTime[0]}s ${map.generationTime[1]}ms`);
    }
  }

  private static getDefaultConfig(): IDungeonConfig {
    return {
      levels: {
        amount: 1,
      },
      maps: {
        rooms: { min: 45, max: 60 },
        size: {
          width: { min: 128, max: 256 },
          height: { min: 128, max: 256 },
        },
        surface: {
          createStairsUpOnLastLevel: true,
          createStairsDownOnLastLevel: true,
          obstacles: { do: true, fromLevel: 1, separation: 5, amount: { min: 0, max: 35 } },
          chests: { do: true, fromLevel: 1, separation: 5, amount: { min: 0, max: 5 } },
        },
        allowRoomsToOverlap: false,
      },
      rooms: {
        size: { minWidth: 15, maxWidth: 30, minHeight: 15, maxHeight: 35 },
        min: 20,
        chances: {
          maxRange: 100,
          store: { min: 0, max: 5 },
          pvpArea: { min: 0, max: 2 },
          nonPvpArea: { min: 0, max: 0 },
          bossArea: { min: 0, max: 10 },
        },
      },
      dungeon: {
        maxTries: 100,
        type: DungeonType.WITHOUT_CORRIDORS,
      },
    };
  }

}
