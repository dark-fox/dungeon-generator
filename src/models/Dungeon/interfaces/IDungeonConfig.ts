import { DungeonType } from '../DungeonType';

export interface ILevelsSettings {
  amount: number;
}

export interface IRangeSettings {
  min: number;
  max: number;
}

export interface IMapSizeSettings {
  width: IRangeSettings;
  height: IRangeSettings;
}

export interface IMapSurfaceElementSettings {
  do: boolean;
  fromLevel: number;
  separation: number;
  amount: IRangeSettings;
}

export interface IMapSurfaceSettings {
  createStairsUpOnLastLevel: boolean;
  createStairsDownOnLastLevel: boolean;
  obstacles: IMapSurfaceElementSettings;
  chests: IMapSurfaceElementSettings;
}

export interface IMapSettings {
  rooms: IRangeSettings;
  size: IMapSizeSettings;
  surface: IMapSurfaceSettings;
  allowRoomsToOverlap: boolean;
}

export interface IRoomSizeSettings {
  minWidth: number;
  maxWidth: number;
  minHeight: number;
  maxHeight: number;
}

export interface IRoomChancesSettings {
  maxRange: number;
  store: IRangeSettings;
  pvpArea: IRangeSettings;
  nonPvpArea: IRangeSettings;
  bossArea: IRangeSettings;
}

export interface IRoomSettings {
  size: IRoomSizeSettings;
  min: number;
  chances: IRoomChancesSettings;
}

export interface IDungeonSettings {
  maxTries: number;
  type: DungeonType;
}

export interface IDungeonConfig {
  levels: ILevelsSettings;
  maps: IMapSettings;
  rooms: IRoomSettings;
  dungeon: IDungeonSettings;
}
