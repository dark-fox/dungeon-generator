import { IDungeonConfig } from './IDungeonConfig';
import { IGeneratedMap } from '../../Map/interfaces/IGeneratedMap';
import { ITimer } from '../../Timer/interfaces/ITimer';
import { IMapConfig } from '../../Map/interfaces/IMapConfig';
import { IRandom } from '../../Random/interfaces/IRandom';

export interface IDungeon {
  config: IDungeonConfig;
  Timer: ITimer;
  Random: IRandom;

  maps: IGeneratedMap[];

  setConfig(config: IDungeonConfig): this;
  generate(): this;
  generateMapConfig(currentLevel: number): IMapConfig;
  getGenerationTime(): string;
  getMaps(): IGeneratedMap[];
}
