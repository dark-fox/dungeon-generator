import { IRandom } from './interfaces/IRandom';

export class Random implements IRandom {
  public integer(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

}
