export interface IRandom {
  integer(min: number, max: number): number;
}
