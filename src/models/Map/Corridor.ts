//Interfaces
import { ICorridor } from './interfaces/ICorridor';
import { IAxis } from './interfaces/IMap';

//Models
import { MapExtender } from './MapExtender';

export class Corridor extends MapExtender implements ICorridor {
  private door: IAxis;

  public setDoor(door: IAxis): this {
    this.door = door;
    return this;
  }

  public create(): void {

  }

}
