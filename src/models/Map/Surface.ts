//Interfaces
import { ISurface, ISurfaceElementsPositions, ISurfaceResults } from './interfaces/ISurface';
import { ISurfaceConfig, ISurfaceDoConfig, ISurfaceDoElementConfig } from './interfaces/ISurfaceConfig';
import { IAxis, IMapSize } from './interfaces/IMap';
import { IRandom } from '../Random/interfaces/IRandom';
import { ICoordinate } from './interfaces/ICoordinate';
import { IClosestTiles } from './interfaces/IClosestTiles';

//Models
import { MapExtender } from './MapExtender';
import { Random } from '../Random/Random';
import { Coordinate } from './Coordinate';
import { surfaceTiles, Tiles } from '../Tiles/Tiles';

//Tools
import { includes, has } from 'lodash';

export class Surface extends MapExtender implements ISurface {
  private mapSize: IMapSize;
  private doConfig: ISurfaceDoConfig;
  private config: ISurfaceConfig;

  private Random: IRandom;
  private Coordinate: ICoordinate;

  public constructor() {
    super();

    this.Random = new Random;
    this.Coordinate = new Coordinate;
  }

  public setMapSize(mapSize: IMapSize): this {
    this.mapSize = mapSize;
    return this;
  }

  public setDo(doConfig: ISurfaceDoConfig): this {
    this.doConfig = doConfig;
    return this;
  }

  public fill(): void {
    this.validate();
    this.generateEnvironmentConfig();

    if (this.doConfig.stairsUp) {
      this.createElement(Tiles.STAIRS_UP, this.doConfig.stairsUp, false);
    }

    if (this.doConfig.stairsDown) {
      this.createElement(Tiles.STAIRS_DOWN, this.doConfig.stairsDown, false);
    }

    if (this.doConfig.obstacles.do) {
      this.createElement(Tiles.OBSTACLE, this.doConfig.obstacles);
    }

    if (this.doConfig.chests.do) {
      this.createElement(Tiles.CHEST, this.doConfig.chests);
    }
  }

  public get(): ISurfaceResults {
    return {
      map: this.map,
    };
  }

  private createElement(tile: Tiles, settings: ISurfaceDoElementConfig, possibleSpotIsOnlyFloor: boolean = false): void {
    this.Coordinate.setMap(this.map);

    const positions: IAxis[] = this.getPossiblePositions(settings.separation, settings.amount * 3, possibleSpotIsOnlyFloor);
    let indexes: number[] = [];

    for (let index: number = 0; index < settings.amount; index++) {
      let element: IAxis;
      let position: number;

      do {
        position = this.Random.integer(0, positions.length - 1);

        if (!has(indexes, index)) {
          element = positions[position];

          if (has(element, 'x') && has(element, 'y') && this.Coordinate.hasSpot(element.x, element.y)) {
            indexes.push(index);
            this.map[element.y][element.x] = tile;
          }
        }
      } while(!has(indexes, index) || index > settings.amount + 50);
    }
  }

  private getPossiblePositions(spacing: number, pointsOfInterest: number, possibleSpotIsOnlyFloor: boolean = false): IAxis[] {
    let positions: IAxis[] = [];

    for (let y: number = 2; y <= this.mapSize.height; y++) {
      for (let x: number = 1; x <= Math.floor(this.mapSize.width / spacing); x++) {
        if (!this.Coordinate.hasSpot(x, y)) {
          continue;
        }

        x = x + spacing;

        const point: IAxis = { x: x, y: y };
        const isCorrectSpot: boolean = possibleSpotIsOnlyFloor
          ? this.Coordinate.getSpotTile(point.x, point.y) === Tiles.FLOOR
          : includes(surfaceTiles, this.Coordinate.getSpotTile(point.x, point.y));

        if (isCorrectSpot) {
          const closest: IClosestTiles = this.Coordinate.getClosestTiles(point, 1);
          const areClosestSpotCorrect: boolean = possibleSpotIsOnlyFloor
            ? closest.left === Tiles.FLOOR && closest.top === Tiles.FLOOR && closest.right === Tiles.FLOOR && closest.bottom === Tiles.FLOOR
            : includes(surfaceTiles, closest.left) && includes(surfaceTiles, closest.top) && includes(surfaceTiles, closest.right) && includes(surfaceTiles, closest.bottom);

          if (areClosestSpotCorrect && this.Coordinate.hasSpot(point.x, point.y)) {
            positions.push(point);
          }

          if (positions.length === pointsOfInterest) {
            break;
          }
        }
      }
    }

    return positions;
  }

  private generateEnvironmentConfig(): void {
    this.config = {
      do: {
        stairsUp: this.doConfig.stairsUp,
        stairsDown: this.doConfig.stairsDown,
        obstacles: this.doConfig.obstacles,
        chests: this.doConfig.chests,
      }
    };
  }

  private validate(): void {
    if (typeof this.map === 'undefined') {
      throw 'Surface: Map is not defined. User Surface.setMap() method before .fill().';
    }

    if (typeof this.mapSize === 'undefined') {
      throw 'Surface: Map size is not defined. User Surface.setMapSize() method before .fill().';
    }

    if (typeof this.doConfig === 'undefined') {
      throw 'Surface: Do config is not defined. User Surface.setDo() method before .fill().';
    }
  }

}