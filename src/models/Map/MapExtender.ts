import { IMapExtender } from './interfaces/IMapExtender';
import { Tiles } from '../Tiles/Tiles';

export abstract class MapExtender implements IMapExtender {
  protected map: Tiles[][];

  public setMap(map: Tiles[][]): void {
    this.map = map;
  }

}
