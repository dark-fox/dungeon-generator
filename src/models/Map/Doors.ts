//Interfaces
import { IDoors, IDoorsResults, IWalls } from './interfaces/IDoors';
import { ICoordinate, IWallClosestTiles } from './interfaces/ICoordinate';
import { IRegisteredRoom } from './interfaces/IRegisteredRoom';
import { IAxis } from './interfaces/IMap';
import { IRandom } from '../Random/interfaces/IRandom';

//Models
import { MapExtender } from './MapExtender';
import { Coordinate } from './Coordinate';
import { surfaceTiles, Tiles } from '../Tiles/Tiles';
import { Random } from '../Random/Random';

//Tools
import { filter, includes } from 'lodash';

export class Doors extends MapExtender implements IDoors {
  private maxTries: number = 100;
  private registeredRooms: IRegisteredRoom[];
  private placedDoors: number = 0;
  private doors: IAxis[] = [];

  private Random: IRandom;
  private Coordinate: ICoordinate;

  public constructor() {
    super();

    this.Random = new Random;
    this.Coordinate = new Coordinate;
  }

  public setMaxTries(maxTries: number): this {
    this.maxTries = maxTries;
    return this;
  }

  public setRooms(registeredRooms: IRegisteredRoom[]): this {
    this.registeredRooms = registeredRooms;
    return this;
  }

  public create(): void {
    this.validate();

    const placedRooms: number = this.registeredRooms.length;

    for (let room: number = 0; room < placedRooms; room++) {
      const registeredRoom: IRegisteredRoom = this.registeredRooms[room];
      const possibleDoors: number = this.Random.integer(1, 4);

      this.Coordinate.setMap(this.map);

      for (let door: number = 1; door <= possibleDoors; door++) {
        this.tryCreateDoor(registeredRoom);
      }
    }
  }

  public get(): IDoorsResults {
    return {
      placedDoors: this.placedDoors,
      doors: this.doors,
      map: this.map,
    };
  }

  private getRoomWalls(registeredRoom: IRegisteredRoom): IAxis[] {
    let walls: IAxis[] = [];

    for (let y: number = registeredRoom.yStart; y <= registeredRoom.yEnd; y++) {
      for (let x: number = registeredRoom.xStart; x <= registeredRoom.xEnd; x++) {
        if (this.map[y][x] === Tiles.WALL) {
          walls.push({x, y})
        }
      }
    }

    return walls;
  }

  private tryCreateDoor(registeredRoom: IRegisteredRoom, loop: number = 0): IAxis|boolean {
    const roomsWalls: IAxis[] = this.getRoomWalls(registeredRoom);
    const walls: IWalls = {
      left: filter(roomsWalls, { x: roomsWalls[0].x }),
      top: filter(roomsWalls, { y: roomsWalls[0].y }),
      right: filter(roomsWalls, { x: roomsWalls[roomsWalls.length - 1].x }),
      bottom: filter(roomsWalls, { y: roomsWalls[roomsWalls.length - 1].y }),
      width: registeredRoom.xEnd - registeredRoom.xStart,
      height: registeredRoom.yEnd - registeredRoom.yStart,
    };

    this.tryPutDoor([walls.left, walls.top, walls.right, walls.bottom]);

    return false;
  }

  private tryPutDoor(walls: IAxis[][]): void {
    walls.forEach((wall: IAxis[]) => {
      const wallLength: number = wall.length;
      const doorPosition: number = this.Random.integer(0, wallLength - 1);
      const point: IAxis = wall[doorPosition];
      let loop: number = 0;

      do {
        if (this.Coordinate.isPointDoubleWallBetweenRooms(point)) {
          this.putDoubleDoor(point);
          break;
        } else if (this.Coordinate.isPointSingleWallBetweenRooms(point)) {
          this.putSingleDoor(point);
          break;
        }

        loop++;
      } while (loop <= this.maxTries);
    });
  }

  private putDoubleDoor(point: IAxis): void {
    const closest: IWallClosestTiles = this.Coordinate.getWallClosestTiles(point);
    let secondPoint: IAxis = point;

    if (closest.first.top === Tiles.WALL && includes(surfaceTiles, closest.second.top) && includes(surfaceTiles, closest.first.bottom)) {
      secondPoint = { x: point.x, y: point.y + 1 };
    } else if (closest.first.bottom === Tiles.WALL && includes(surfaceTiles, closest.second.bottom) && includes(surfaceTiles, closest.first.top)) {
      secondPoint = { x: point.x, y: point.y - 1 };
    } else if (closest.first.left === Tiles.WALL && includes(surfaceTiles, closest.second.left) && includes(surfaceTiles, closest.first.right)) {
      secondPoint = { x: point.x - 1, y: point.y };
    } else if (closest.first.right === Tiles.WALL && includes(surfaceTiles, closest.second.right) && includes(surfaceTiles, closest.first.left)) {
      secondPoint = { x: point.x + 1, y: point.y };
    }

    this.map[point.y][point.x] = Tiles.DOOR;
    this.registerDoor(point);

    this.map[secondPoint.y][secondPoint.x] = Tiles.DOOR;
    this.registerDoor(secondPoint);
  }


  private putSingleDoor(point: IAxis): void {
    this.map[point.y][point.x] = Tiles.DOOR;
    this.registerDoor(point);
  }

  private registerDoor(door: IAxis): void {
    this.placedDoors++;
    this.doors.push({
      x: door.x,
      y: door.y,
    });
  }

  private validate(): void {
    if (typeof this.map === 'undefined') {
      throw 'Doors: Map is not defined. User Doors.setMap() method before .create().';
    }

    if (typeof this.maxTries === 'undefined') {
      throw 'Doors: Max tries parameter is not defined. User Doors.setMaxTries() method before .create().';
    }

    if (typeof this.registeredRooms === 'undefined') {
      throw 'Doors: Registered rooms are not defined. User Doors.setRooms() method before .create().';
    }
  }

}