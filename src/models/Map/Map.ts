//Interfaces
import { IGeneratedMap } from './interfaces/IGeneratedMap';
import { IMapConfig } from './interfaces/IMapConfig';
import { IAxis, IMap } from './interfaces/IMap';
import { IRegisteredRoom } from './interfaces/IRegisteredRoom';
import { ISurface, ISurfaceResults } from './interfaces/ISurface';
import { ICorridor } from './interfaces/ICorridor';
import { ICoordinate } from './interfaces/ICoordinate';
import { IRooms, IRoomsResults } from './interfaces/IRooms';
import { IDoors, IDoorsResults } from './interfaces/IDoors';
import { ISurfaceDoConfig } from './interfaces/ISurfaceConfig';

import { ITimer } from '../Timer/interfaces/ITimer';
import { IRandom } from '../Random/interfaces/IRandom';

//Models
import { Corridor } from './Corridor';
import { Timer } from '../Timer/Timer';
import { Random } from '../Random/Random';
import { Tiles } from '../Tiles/Tiles';
import { Coordinate } from './Coordinate';
import { Rooms } from './Rooms';
import { Doors } from './Doors';
import { Surface } from './Surface';

export class Map implements IMap{
  public config: IMapConfig;
  public level: number;

  public Timer: ITimer;
  public Random: IRandom;

  private Coordinate: ICoordinate;
  private Rooms: IRooms;
  private Doors: IDoors;
  private Surface: ISurface;
  private Corridor: ICorridor;

  private map: Tiles[][] = [];
  private placedRooms: number = 0;
  private registeredRooms: IRegisteredRoom[] = [];
  private placedDoors: number = 0;
  private doors: IAxis[] = [];

  public constructor(level: number, config: IMapConfig) {
    this.level = level;
    this.config = config;

    this.Timer = new Timer;
    this.Random = new Random;

    this.Coordinate = new Coordinate;
    this.Rooms = new Rooms;
    this.Doors = new Doors;
    this.Surface = new Surface;
    this.Corridor = new Corridor;
  }

  public generate(): this {
    this.Timer.start();

    this.createEmptyMap();
    this.createRooms();
    this.createDoors();
    this.fillSurface();

    if (this.config.doCorridors) {
      this.createCorridors();
    }

    this.Timer.end();

    return this;
  }

  public getMap(): IGeneratedMap {
    return {
      level: this.level,
      generationTime: this.Timer.getDifference(),
      rooms: {
        total: this.config.rooms,
        placed: this.placedRooms,
        doors: this.placedDoors,
      },
      map: this.map,
    };
  }

  public getRegisteredRooms(): IRegisteredRoom[] {
    return this.registeredRooms;
  }

  private createEmptyMap(): void {
    for (let y: number = 0; y <= this.config.mapSize.height; y++) {
      this.map.push([]);

      for (let x: number = 0; x <= this.config.mapSize.width; x++) {
        this.map[y].push(Tiles.EMPTY);
      }
    }
  }

  private createRooms(loop: number = 0): void {
    this.Rooms.setMap(this.map);
    this.Rooms.setConfig(this.config).create();

    this.placeRooms(loop);
  }

  private placeRooms(loop: number = 0): void {
    const rooms: IRoomsResults = this.Rooms.get();

    if (rooms.placedRooms < this.config.minimalRoomsPlaced && loop <= this.config.maxTries) {
      loop++;

      this.Rooms = new Rooms;
      this.createRooms(loop);
    } else {
      this.placedRooms = rooms.placedRooms;
      this.registeredRooms = rooms.registeredRooms;
      this.map = rooms.map;
    }
  }

  private createDoors(): void {
    this.Doors.setMap(this.map);
    this.Doors.setRooms(this.registeredRooms).create();

    this.placeDoors();
  }

  private placeDoors(): void {
    const doors: IDoorsResults = this.Doors.get();

    this.placedDoors = doors.placedDoors;
    this.doors = doors.doors;
    this.map = doors.map;
  }

  private fillSurface(): void {
    const doConfig: ISurfaceDoConfig = {
      stairsUp: {
        do: this.config.surface.createStairsUpOnFirstLevel ? true : this.config.currentMap > 1,
        separation: 5,
        amount: 1,
      },
      stairsDown: {
        do: this.config.surface.createStairsDownOnLastLevel ? true : this.config.currentMap < this.config.mapsToCreate,
        separation: 5,
        amount: 1,
      },
      obstacles: {
        do: this.config.surface.obstacles.do && this.config.surface.obstacles.fromLevel >= this.config.currentMap,
        separation: this.config.surface.obstacles.separation,
        amount: this.Random.integer(this.config.surface.obstacles.amount.min, this.config.surface.obstacles.amount.max),
      },
      chests: {
        do: this.config.surface.chests.do && this.config.surface.chests.fromLevel >= this.config.currentMap,
        separation: this.config.surface.chests.separation,
        amount: this.Random.integer(this.config.surface.chests.amount.min, this.config.surface.chests.amount.max),
      },
    };

    this.Surface.setMap(this.map);
    this.Surface.setMapSize(this.config.mapSize).setDo(doConfig).fill();

    this.placeSurfaceElements();
  }

  private placeSurfaceElements(): void {
    const surface: ISurfaceResults = this.Surface.get();

    this.map = surface.map;
  }

  private createCorridors(): void {
    for (let doorNumber: number = 0; doorNumber < this.placedDoors; doorNumber++) {
      const door: IAxis = this.doors[doorNumber];

      this.Corridor.setMap(this.map);
      this.Corridor.setDoor(door).create();
    }
  }

}
