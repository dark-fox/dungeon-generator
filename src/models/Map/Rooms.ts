//Interfaces
import { IRooms, IRoomsResults } from './interfaces/IRooms';
import { IGeneratedRoom } from '../Room/interfaces/IGeneratedRoom';
import { IRoom } from '../Room/interfaces/IRoom';
import { IRegisteredRoom } from './interfaces/IRegisteredRoom';

import { IAxis } from './interfaces/IMap';
import { ICoordinate } from './interfaces/ICoordinate';

import { IMapConfig } from './interfaces/IMapConfig';
import { IRoomConfig } from '../Room/interfaces/IRoomConfig';
import { IRandom } from '../Random/interfaces/IRandom';

//Models
import { MapExtender } from './MapExtender';
import { Room } from '../Room/Room';
import { Tiles } from '../Tiles/Tiles';
import { RoomType } from '../Room/RoomType';
import { Random } from '../Random/Random';
import { Coordinate } from './Coordinate';
import { RoomsConnections } from './RoomsConnections';

export class Rooms extends MapExtender implements IRooms {
  private config: IMapConfig;

  private placedRooms: number = 0;
  private registeredRooms: IRegisteredRoom[] = [];
  private lastRoomConnectionDirection: number = -1;

  private Random: IRandom;
  private Coordinate: ICoordinate;

  public constructor() {
    super();

    this.Random = new Random;
    this.Coordinate = new Coordinate;
  }

  public setConfig(config: IMapConfig): this {
    this.config = config;
    return this;
  }

  public create(): void {
    let loop: number = 0;

    this.validate();
    this.Coordinate.setMap(this.map);

    for (let currentRoom: number = 1; currentRoom <= this.config.rooms; currentRoom++) {
      this.doCreate();
    }

    while (this.placedRooms < this.config.minimalRoomsPlaced && loop < this.config.maxTries) {
      this.doCreate();
      loop++;
    }
  }

  public get(): IRoomsResults {
    return {
      placedRooms: this.placedRooms,
      registeredRooms: this.registeredRooms,
      map: this.map,
    };
  }

  public getRegisteredRooms(): IRegisteredRoom[] {
    return this.registeredRooms;
  }

  public getMap(): Tiles[][] {
    return this.map;
  }

  private doCreate(): void {
    const NewRoom: IRoom = new Room(this.generateRoomConfig());
    const room: IGeneratedRoom = NewRoom.generate().getRoom();

    this.tryPlaceRoom(room);
  }

  private tryPlaceRoom(room: IGeneratedRoom, loop: number = 0): boolean {
    let startPosition: IAxis, endPosition: IAxis;

    if (this.config.doCorridors || this.placedRooms === 0) {
      startPosition = this.getRandomPosition();
      endPosition = {
        x: startPosition.x + room.size.width,
        y: startPosition.y + room.size.height,
      };
    } else {
      const closestRoom: IRegisteredRoom = this.registeredRooms[this.Random.integer(0, (this.placedRooms - 1))];

      let maxTries: number = this.config.maxTries;
      let newRoomLocation: number;

      do {
        do {
          newRoomLocation = this.Random.integer(0, 4);
        } while (this.lastRoomConnectionDirection == newRoomLocation);

        switch (newRoomLocation) {
          case RoomsConnections.LEFT:
            startPosition = {
              x: closestRoom.xStart - 1 - room.size.width,
              y: closestRoom.yStart,
            };
            endPosition = {
              x: closestRoom.xStart - 1,
              y: startPosition.y + room.size.height,
            };
            break;
          case RoomsConnections.TOP:
            startPosition = {
              x: closestRoom.xStart,
              y: closestRoom.yStart - 1 - room.size.height,
            };
            endPosition = {
              x: startPosition.x + room.size.width,
              y: closestRoom.yStart - 1,
            };
            break;
          case RoomsConnections.RIGHT:
            startPosition = {
              x: closestRoom.xEnd + 1,
              y: closestRoom.yStart,
            };
            endPosition = {
              x: closestRoom.xEnd + 1 + room.size.width,
              y: startPosition.y + room.size.height,
            };
            break;
          case RoomsConnections.BOTTOM:
          default:
            startPosition = {
              x: closestRoom.xStart,
              y: closestRoom.yEnd + 1,
            };
            endPosition = {
              x: startPosition.x + room.size.width,
              y: closestRoom.yEnd + 1 + room.size.height,
            };
            break;
        }

        maxTries++;
      } while (!this.isPositionAvailable(endPosition) && maxTries <= this.config.maxTries);

      this.lastRoomConnectionDirection = newRoomLocation;
    }

    if (this.config.allowRoomsToOverlap && this.isPositionAvailable(startPosition) && this.isPositionAvailable(endPosition)) {
      return this.placeRoom(room, startPosition, endPosition);
    } else if (!this.config.allowRoomsToOverlap && this.isRoomDoesNotOverlapOtherOne(room, startPosition, endPosition)) {
      return this.placeRoom(room, startPosition, endPosition);
    } else if (loop <= this.config.maxTries) {
      loop++;
      this.tryPlaceRoom(room, loop);
    }

    return false;
  }

  private isRoomDoesNotOverlapOtherOne(room: IGeneratedRoom, startPosition: IAxis, endPosition: IAxis, loop: number = 0): boolean {
    if (this.isPositionAvailable(startPosition) && this.isPositionAvailable(endPosition)) {
      for (let y: number = 0; y <= room.size.height; y++) {
        for (let x: number = 0; x <= room.size.width; x++) {
          let map: IAxis = {
            x: startPosition.x + x,
            y: startPosition.y + y,
          };

          if (this.map[map.y][map.x] !== Tiles.EMPTY) {
            return false;
          }
        }
      }

      return true;
    } else if (loop <= this.config.maxTries) {
      loop++;
      return this.isRoomDoesNotOverlapOtherOne(room, startPosition, endPosition, loop);
    }

    return false;
  }

  private placeRoom(room: IGeneratedRoom, startPosition: IAxis, endPosition: IAxis): boolean {
    for (let y: number = 0; y <= room.size.height; y++) {
      for (let x: number = 0; x <= room.size.width; x++) {
        let map: IAxis = {
          x: startPosition.x + x,
          y: startPosition.y + y,
        };

        this.map[map.y][map.x] = room.room[y][x];
      }
    }

    this.registerRoom(startPosition, endPosition);

    return true;
  }

  private generateRoomType(): RoomType {
    const types: { [n: number]: number; } = {
      [RoomType.STORE]: this.config.chances.store,
      [RoomType.PVP_AREA]: this.config.chances.pvpArea,
      [RoomType.NON_PVP_AREA]: this.config.chances.nonPvpArea,
      [RoomType.BOSS]: this.config.chances.bossArea,
    };

    let roomType: number = RoomType.DEFAULT;

    for (let type in types) {
      const chance: number = this.Random.integer(1, this.config.chances.maxRange);
      const typeChance: number = types[type];

      if (typeChance > 0 && chance < typeChance) {
        roomType = Number(type);
      }
    }

    return roomType;
  }

  private generateRoomConfig(): IRoomConfig {
    return {
      size: {
        width: this.Random.integer(this.config.roomsSize.minWidth, this.config.roomsSize.maxWidth),
        height: this.Random.integer(this.config.roomsSize.minHeight, this.config.roomsSize.minHeight),
      },
      type: this.generateRoomType(),
    };
  }

  private isPositionAvailable(position: IAxis): boolean {
    return this.Coordinate.getSpotTile(position.x, position.y) === Tiles.EMPTY;
  }

  private getRandomPosition(): IAxis {
    return {
      x: this.Random.integer(0, this.config.mapSize.width),
      y: this.Random.integer(0, this.config.mapSize.height),
    };
  }

  private registerRoom(startPosition: IAxis, endPosition: IAxis): void {
    this.placedRooms++;
    this.registeredRooms.push({
      xStart: startPosition.x,
      yStart: startPosition.y,
      xEnd: endPosition.x,
      yEnd: endPosition.y,
    });
  }

  private validate(): void {
    if (typeof this.map === 'undefined') {
      throw 'Rooms: Map is not defined. User Rooms.setMap() method before .create().';
    }

    if (typeof this.config === 'undefined') {
      throw 'Rooms: Map config is not defined. User Rooms.setConfig() method before .create().';
    }
  }

}
