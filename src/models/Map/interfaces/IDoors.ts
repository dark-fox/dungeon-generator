import { IMapExtender } from './IMapExtender';
import { IRegisteredRoom } from './IRegisteredRoom';
import { IAxis } from './IMap';
import { Tiles } from '../../Tiles/Tiles';

export interface IDoorsResults {
  placedDoors: number;
  doors: IAxis[];
  map: Tiles[][];
}

export interface IWalls {
  left: IAxis[];
  top: IAxis[];
  bottom: IAxis[];
  right: IAxis[];
  width: number;
  height: number;
}

export interface IDoors extends IMapExtender {
  setMaxTries(maxTries: number): this;
  setRooms(registeredRooms: IRegisteredRoom[]): this;
  create(): void;
  get(): IDoorsResults;
}