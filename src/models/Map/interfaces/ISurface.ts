import { IMapExtender } from './IMapExtender';
import { IAxis, IMapSize } from './IMap';
import { ISurfaceDoConfig } from './ISurfaceConfig';
import { Tiles } from '../../Tiles/Tiles';

export interface ISurfaceResults {
  map: Tiles[][];
}

export interface ISurfaceStairsPositions {
  up: IAxis|boolean;
  down: IAxis|boolean;
}

export interface ISurfaceElementsPositions {
  stairs: ISurfaceStairsPositions;
}

export interface ISurface extends IMapExtender {
  setMapSize(mapSize: IMapSize): this;
  setDo(doConfig: ISurfaceDoConfig): this;
  fill(): void;
  get(): ISurfaceResults;
}