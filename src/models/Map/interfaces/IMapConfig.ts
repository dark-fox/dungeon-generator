import { IMapSurfaceElementSettings, IRoomSizeSettings } from '../../Dungeon/interfaces/IDungeonConfig';
import { IMapSize } from './IMap';

export interface IChances {
  maxRange: number;
  store: number;
  pvpArea: number;
  nonPvpArea: number;
  bossArea: number;
}

export interface ISurface {
  createStairsUpOnFirstLevel: boolean;
  createStairsDownOnLastLevel: boolean;
  obstacles: IMapSurfaceElementSettings;
  chests: IMapSurfaceElementSettings;
}

export interface IMapConfig {
  currentMap: number;
  mapsToCreate: number;
  rooms: number;
  roomsSize: IRoomSizeSettings;
  minimalRoomsPlaced: number;
  mapSize: IMapSize;
  chances: IChances;
  surface: ISurface;
  maxTries: number;
  allowRoomsToOverlap: boolean;
  doCorridors: boolean;
}
