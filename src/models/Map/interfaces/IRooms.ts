import { IMapExtender } from './IMapExtender';
import { IMapConfig } from './IMapConfig';
import { IRegisteredRoom } from './IRegisteredRoom';
import { Tiles } from '../../Tiles/Tiles';

export interface IRoomsResults {
  placedRooms: number,
  registeredRooms: IRegisteredRoom[],
  map: Tiles[][],
}

export interface IRooms extends IMapExtender {
  setConfig(config: IMapConfig): this;
  create(): void;
  get(): IRoomsResults;
  getRegisteredRooms(): IRegisteredRoom[];
  getMap(): Tiles[][];
}
