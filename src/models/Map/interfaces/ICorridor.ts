import { IMapExtender } from './IMapExtender';
import { IAxis } from './IMap';

export interface ICorridor extends IMapExtender{
  setDoor(door: IAxis): this;
  create(): void;
}
