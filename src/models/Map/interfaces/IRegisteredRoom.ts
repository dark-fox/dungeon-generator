export interface IRegisteredRoom {
  xStart: number;
  yStart: number;
  xEnd: number;
  yEnd: number;
}
