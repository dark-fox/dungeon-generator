//Interfaces
import { IMapExtender } from './IMapExtender';
import { IClosestTiles } from './IClosestTiles';
import { IAxis } from './IMap';

//Enums
import { Tiles } from '../../Tiles/Tiles';

export interface IWallClosestTiles {
  first: IClosestTiles;
  second: IClosestTiles;
}

export interface ICoordinate extends IMapExtender {
  getClosestTiles(point: IAxis, range: number): IClosestTiles;
  getWallClosestTiles(point: IAxis): IWallClosestTiles;
  isPointWallBetweenRooms(point: IAxis): boolean;
  isPointSingleWallBetweenRooms(point: IAxis): boolean;
  isPointDoubleWallBetweenRooms(point: IAxis): boolean;
  isPointCorner(point: IAxis, tile?: Tiles): boolean;
  getSpotTile(x: number, y: number): Tiles|boolean;
  hasSpot(x: number, y: number): boolean;
}
