//Interfaces
import { ITimer } from '../../Timer/interfaces/ITimer';
import { IRandom } from '../../Random/interfaces/IRandom';
import { IGeneratedMap } from './IGeneratedMap';
import { IMapConfig } from './IMapConfig';
import { IRegisteredRoom } from './IRegisteredRoom';

export interface IAxis {
  x: number;
  y: number;
}

export interface IMapSize {
  width: number;
  height: number;
}

export interface IMap {
  config: IMapConfig;
  level: number;

  Timer: ITimer;
  Random: IRandom;

  generate(): this;
  getMap(): IGeneratedMap;
  getRegisteredRooms(): IRegisteredRoom[];
}
