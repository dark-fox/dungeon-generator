import { Tiles } from '../../Tiles/Tiles';

export interface IGeneratedMapRooms {
  total: number;
  placed: number;
  doors: number;
}

export interface IGeneratedMap {
  level: number
  generationTime: [number, number];
  rooms: IGeneratedMapRooms;
  map: Tiles[][];
}
