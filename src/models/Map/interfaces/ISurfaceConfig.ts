export interface ISurfaceDoElementConfig {
  do: boolean;
  separation: number;
  amount: number;
}

export interface ISurfaceDoConfig {
  stairsUp: ISurfaceDoElementConfig;
  stairsDown: ISurfaceDoElementConfig;
  obstacles: ISurfaceDoElementConfig;
  chests: ISurfaceDoElementConfig;
}

export interface ISurfaceConfig {
  do: ISurfaceDoConfig;
}