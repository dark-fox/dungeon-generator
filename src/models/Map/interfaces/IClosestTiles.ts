import { Tiles } from '../../Tiles/Tiles';

export interface IClosestTiles {
  left: Tiles|boolean;
  top: Tiles|boolean;
  right: Tiles|boolean;
  bottom: Tiles|boolean;
  center: Tiles;
}
