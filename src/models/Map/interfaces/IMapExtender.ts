import { Tiles } from '../../Tiles/Tiles';

export interface IMapExtender {
  setMap(map: Tiles[][]): void;
}
