//Interfaces
import { ICoordinate, IWallClosestTiles } from './interfaces/ICoordinate';
import { IClosestTiles } from './interfaces/IClosestTiles';
import { IAxis } from './interfaces/IMap';

//Models
import { MapExtender } from './MapExtender';

//Enums
import { surfaceTiles, Tiles } from '../Tiles/Tiles';

//Tools
import { get, has, includes } from 'lodash';

export class Coordinate extends MapExtender implements ICoordinate {
  public getClosestTiles(point: IAxis, range: number = 1): IClosestTiles {
    this.checkMap();
    return {
      // @ts-ignore
      left: get(this.map, '[' + [point.y] + '][' + [point.x - range] + ']', false),
      // @ts-ignore
      top: get(this.map, '[' + [point.y + range] + '][' + [point.x] + ']', false),
      // @ts-ignore
      right: get(this.map, '[' + [point.y] + '][' + [point.x + range] + ']', false),
      // @ts-ignore
      bottom: get(this.map, '[' + [point.y - range] + '][' + [point.x] + ']', false),
      center: this.map[point.y][point.x],
    }
  }

  public getWallClosestTiles(point: IAxis): IWallClosestTiles {
    return {
      first: this.getClosestTiles(point, 1),
      second: this.getClosestTiles(point, 2),
    };
  }

  public isPointWallBetweenRooms(point: IAxis): boolean {
    return (
      this.isPointDoubleWallBetweenRooms(point)
      || this.isPointSingleWallBetweenRooms(point)
    );
  }

  public isPointSingleWallBetweenRooms(point: IAxis): boolean {
    const closest: IWallClosestTiles = this.getWallClosestTiles(point);
    return (
      (includes(surfaceTiles, closest.first.left) && includes(surfaceTiles, closest.first.right) && closest.first.top === Tiles.WALL && closest.second.top === Tiles.WALL && closest.first.bottom === Tiles.WALL && closest.second.bottom === Tiles.WALL)
      || (includes(surfaceTiles, closest.first.top) && includes(surfaceTiles, closest.first.bottom) && closest.first.left === Tiles.WALL && closest.second.left === Tiles.WALL && closest.first.right === Tiles.WALL && closest.second.right === Tiles.WALL)
    );
  }

  public isPointDoubleWallBetweenRooms(point: IAxis): boolean {
    const closest: IWallClosestTiles = this.getWallClosestTiles(point);
    return (
      (includes(surfaceTiles, closest.first.bottom) && closest.first.top === Tiles.WALL && includes(surfaceTiles, closest.second.top))
      || (includes(surfaceTiles, closest.first.top) && closest.first.bottom === Tiles.WALL && includes(surfaceTiles, closest.second.bottom))
      || (includes(surfaceTiles, closest.first.left) && closest.first.right === Tiles.WALL && includes(surfaceTiles, closest.second.right))
      || (includes(surfaceTiles, closest.first.right) && closest.first.left === Tiles.WALL && includes(surfaceTiles, closest.second.left))
    );
  }

  public isPointCorner(point: IAxis, tile: Tiles = Tiles.WALL): boolean {
    const closest: IClosestTiles = this.getClosestTiles(point);

    return (
      (closest.top === tile && closest.left === tile)
      || (closest.top === tile && closest.right === tile)
      || (closest.bottom === tile && closest.left === tile)
      || (closest.bottom === tile && closest.right === tile)
      || (closest.top === tile && closest.bottom === tile && closest.left === tile)
      || (closest.top === tile && closest.bottom === tile && closest.right === tile)
      || (closest.left === tile && closest.right === tile && closest.top === tile)
      || (closest.left === tile && closest.right === tile && closest.bottom === tile)
      || (closest.top === tile && closest.bottom === tile && closest.left === tile && closest.right === tile)
    );
  }

  public getSpotTile(x: number, y: number): Tiles|boolean {
    this.checkMap();
    return has(this.map, '[' + y + '][' + x + ']') ? this.map[y][x] : false;
  }

  public hasSpot(x: number, y: number): boolean {
    this.checkMap();
    return has(this.map, '[' + y + '][' + x + ']');
  }

  private checkMap(): void {
    if (typeof this.map === 'undefined') {
      console.warn('Coordinate: Map is undefined. Set it using Coordinate.setMap() before another function usage.');
    }
  }

}
