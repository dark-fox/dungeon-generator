export interface ITimer {
  start(): void;
  end(): void;
  getDifference(): [number, number];
  getReadableDifference(): string;
}
