import { ITimer } from './interfaces/ITimer';

export class Timer implements ITimer{
  private ms: number = 1000000;
  private timeStart: [number, number];
  private timeEnd: [number, number];

  public start(): void {
    this.timeStart = process.hrtime();
  }

  public end(): void {
    this.timeEnd = process.hrtime(this.timeStart);
  }

  public getDifference(): [number, number] {
    return [
      this.timeEnd[0],
      this.timeEnd[1] / this.ms,
    ];
  }

  public getReadableDifference(): string {
    return `${this.timeEnd[0]}.${Math.round(this.timeEnd[0] / this.ms)}`;
  }

}
