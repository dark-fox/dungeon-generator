//Interfaces
import { IRoom } from './interfaces/IRoom';
import { IRoomConfig } from './interfaces/IRoomConfig';
import { IGeneratedRoom } from './interfaces/IGeneratedRoom';

import { ITimer } from '../Timer/interfaces/ITimer';
import { IRandom } from '../Random/interfaces/IRandom';

//Models
import { Timer } from '../Timer/Timer';
import { Random } from '../Random/Random';

//Enums
import { Tiles } from '../Tiles/Tiles';
import { RoomType } from './RoomType';

export class Room implements IRoom {
  public config: IRoomConfig;

  public Timer: ITimer;
  public Random: IRandom;

  private room: Tiles[][] = [];

  public constructor(config: IRoomConfig) {
    this.config = config;
    this.Timer = new Timer;
    this.Random = new Random;
  }

  public generate(): this {
    this.Timer.start();

    this.createEmptyRoom();

    this.Timer.end();

    return this;
  }

  public getRoom(): IGeneratedRoom {
    return {
      generationTime: this.Timer.getDifference(),
      room: this.room,
      size: this.config.size,
    };
  }

  private createEmptyRoom(): void {
    for (let y: number = 0; y <= this.config.size.height; y++) {
      this.room.push([]);

      for (let x: number = 0; x <= this.config.size.width; x++) {
        if (x == 0 || x == this.config.size.width || y == 0 || y == this.config.size.height) {
          this.room[y].push(Tiles.WALL);
        } else {
          this.room[y].push(this.getSurfaceTile());
        }
      }
    }
  }

  private getSurfaceTile(): Tiles {
    switch (this.config.type) {
      default:
      case RoomType.DEFAULT:
        return Tiles.FLOOR;
      case RoomType.STORE:
        return Tiles.STORE;
      case RoomType.BOSS:
        return Tiles.BOSS;
      case RoomType.PVP_AREA:
        return Tiles.PVP_AREA;
      case RoomType.NON_PVP_AREA:
        return Tiles.NO_PVP_AREA;
    }
  }

}
