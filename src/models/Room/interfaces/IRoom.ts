import { IRoomConfig } from './IRoomConfig';
import { ITimer } from '../../Timer/interfaces/ITimer';
import { IGeneratedRoom } from './IGeneratedRoom';

export interface IRoom {
  config: IRoomConfig;

  Timer: ITimer;

  generate(): this;
  getRoom():IGeneratedRoom;
}
