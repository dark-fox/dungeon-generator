import { Tiles } from '../../Tiles/Tiles';
import { IRoomSize } from './IRoomConfig';

export interface IGeneratedRoom {
  generationTime: [number, number];
  room: Tiles[][];
  size: IRoomSize;
}
