import { RoomType } from '../RoomType';

export interface IRoomSize {
  width: number;
  height: number;
}

export interface IRoomConfig {
  size: IRoomSize;
  type: RoomType;
}
