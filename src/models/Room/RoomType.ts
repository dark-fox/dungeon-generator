export enum RoomType {
  DEFAULT,
  STORE,
  PVP_AREA,
  NON_PVP_AREA,
  BOSS
}
