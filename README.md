# Dungeon Generator

Dungeon Generator is an application written in TypeScript, that provides a randomly generated dungeons based on given config. Result is an array filled with maps. Single map could be read by ie. Phaser Engine.

## Index
1. [Dictionary](#dictionary)
1. [How to run?](#how-to-run)
1. [Map structure](#map-structure)
    1. [What is the meaning of map content?](#what-is-the-meaning-of-map-content)
1. [Understanding of Dungeon Config](#what-is-the-meaning-of-map-content)


## Dictionary
**Dungeon** - final result of this application. An array filled with *maps*.

**Map** - single, randomly generated surface for a game. Is filled with rooms. If you want to read more detail about map (and dungeon) structure - read **Output structure** section of this doc. Finally - map is a surface to play.

**Room** - part of map. Most random element in creation.

## How to run?
to be written...

## Map structure
```javascript
const dungeon = [
  [
    ['.', '.', '.'],
    ['.', '.', '.'],
  ],
  [
    ['#', '#', '#'],
    ['#', '@', '#'],
    ['#', '#', '#'],
  ],
];
```
Given example means, that variable **dungeon** contains two maps. First one (key 0) is filled with empty tiles. Second one contains only one room with size of 3x3 squares. 

Notice, how above example could be translated:
```javascript
const dungeon = [
  [ //start map
    ['.', '.', '.'], // 1st row, y = 0
    [                // 2nd row, y = 1
      '.',  //1st column, x = 0
      '#',  //2nd column, x = 1
      '@',  //3rd column, x = 2
    ],
  ], //end map
];
```
So, for this map, spot `y = 1, x = 2` got content `@`. Spot `y = 0, x = 1` is `.`. **Remember** - due to array structure, `y` is checked first.

#### What is the meaning of map content?
Map is filled with different, single characters called in application as Tiles. Each one has different meaning. In example, `.` is an empty space, `#` stands for a room wall and `@` is a room surface. Full specification could be found in Tiles enum at `app\models\Tiles\Tiles.ts`.

## Understanding of Dungeon Config
Dungeon config is an JSON assignable to `IDungeonConfig` interface that could be found at `app\models\Dungeon\interfaces\IDungeonConfig.ts`. Generator got predefined, default config. It'll be chose when any was set during Dungeon creation.

```javascript
return {
  //start section of dungeon settings
  levels: {  
    amount: 1, //how many levels will had dungeon? or: how many maps will be created?
  },
  //end section of dungeon settings
  
  //start section of map settings
  maps: {
    rooms: { min: 10, max: 20 }, //how many rooms could be tried placed? 10 >= x <= 20
    size: { //map size settings
      width: { min: 500, max: 700 }, //possible ranges for map width
      height: { min: 500, max: 700 }, //possible ranges for map height
    },
    surface: { //map surface settings
      createStairsUpOnLastLevel: true, //should be stairs up create on first map?
      createStairsDownOnLastLevel: true, //should be stairs down create on first map?
      obstacles: { //obstacles settings 
        do: true, //should maps contain obstacles?
        fromLevel: 1, //on which level should be first obstacle?
        separation: 5, //min. separation between obstacles
        amount: { //possible amount of obstacles on map
          min: 0, 
          max: 30, 
        }, 
      },
      chests: { do: true, fromLevel: 1, tilesToOmit: 10, amount: { min: 0, max: 10 } }, //config same as obstacles
    },
    allowRoomsToOverlap: true, //allows rooms in map to overlaps each other, so creations could be more interesting
    dungeon: {
      maxTries: 100, //how many times should generator try
      type: DungeonType.WITHOUT_CORRIDORS, //should rooms be connected with corridors or should be stick to each other?
    },
  },
  //end section of map settings
  
  //start section of rooms settings
  rooms: {
    size: { minWidth: 20, maxWidth: 35, minHeight: 20, maxHeight: 40 }, //how big could be single room?
    min: 20, //minimal amount of placed rooms
    chances: { //chances to create different than default area on map
      maxRange: 100, //generator will be generate number between 1 and maxRange
      store: { min: 0, max: 5 }, //if generated number will be between 0 and 5, store will be created
      pvpArea: { min: 0, max: 2 },
      nonPvpArea: { min: 0, max: 0 },
      bossArea: { min: 0, max: 10 },
    },
  },
  //end section of rooms settings
};
```
